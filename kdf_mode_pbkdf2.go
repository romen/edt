package edt

import (
	"crypto/rand"
	"encoding/binary"
	"fmt"
	"golang.org/x/crypto/pbkdf2"
	"golang.org/x/crypto/sha3"
	"golang.org/x/text/unicode/norm"
	"hash"
)

/* The generic pbkdf2Base KDFMode can be instantiated with different underlying hash functions */
type pbkdf2Base struct {
	h   func() hash.Hash
	nid NID
}

/* Implementation of KDFMode.Key() based on a generic pbkdf2Base:
*
* Input:
*		passphrase	(UTF-8 string, will be normalized and converted into []byte)
*		salt		(random byte sequence >= 8bytes to use as salt)
*		iter		(number of iterations, the higer the more cumbersome/resistant against brute force)
		keyLen		(desired output key length)
* Output:
*		key []byte
*/
func (base *pbkdf2Base) Key(passphrase string, salt []byte, iter, keyLen int) []byte {
	normBytes := norm.NFC.Bytes([]byte(passphrase)) // normalize the input UTF-8 passphrase
	return pbkdf2.Key(normBytes, salt, iter, keyLen, base.h)
}

func (base *pbkdf2Base) GetNID() NID {
	return base.nid
}

// RIterations() returns a randomized appropriate number of iterations to
// perform key derivation with this KDFMode
// For PBKDF2 10000 + [0, 30000]
func (base *pbkdf2Base) RIterations() int {
	b := make([]byte, 2)
	_, err := rand.Read(b)
	if err != nil { // treat this as a fatal error
		panic(fmt.Sprintf("rand.Read failure: %s\n", err.Error()))
	}
	rcount := int(binary.BigEndian.Uint16(b)) % 30000
	return 10000 + rcount
}

// RSalt returns an appropriate random salt octet string
// for PBKDF2 a 64-bit random salt should be sufficient
func (base *pbkdf2Base) RSalt() []byte {
	b := make([]byte, 8)
	_, err := rand.Read(b)
	if err != nil { // treat this as a fatal error
		panic(fmt.Sprintf("rand.Read failure: %s\n", err.Error()))
	}
	return b
}

// newPBKDF2_SHA3_512() is a private constructor for a KDFMode based on PBKDF2
// using SHA-3 512 as the underlying hash function
func newPBKDF2_SHA3_512() KDFMode {
	return &pbkdf2Base{sha3.New512, NID_PBKDF2_SHA3_512}
}
