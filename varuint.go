package edt

import (
	"encoding/binary"
	"fmt"
)

type varUInt uint64

func (v varUInt) ByteLength() (i int) {
	t := uint64(v)
	if t == 0 {
		return 1
	}
	for t > 0 {
		t = (t >> 8)
		i++
	}
	return
}

func (v varUInt) Serialize() []byte {
	byteLength := v.ByteLength()
	// "Compress" varUInt serialization to use the smaller possible amount of bytes:
	tmp := make([]byte, 8)
	binary.BigEndian.PutUint64(tmp, uint64(v)) // to BigEndian

	// 1st byte is the length (>= 1 , <= 8), followed by the actual integer
	ret := []byte{byte(byteLength)}
	// only store the last v.ByteLength() bytes
	tmp = tmp[len(tmp)-byteLength:]
	return append(ret, tmp...)
}

func (v *varUInt) Parse(in []byte) (uint64, error) {
	var consumed uint64
	l := int(in[0])
	if l < 1 || l > 8 || l+1 > len(in) {
		return 0, fmt.Errorf("Invalid varUInt lenght: %#x", in[0])
	}
	consumed++
	tmp := make([]byte, 8)
	copy(tmp[len(tmp)-l:], in[1:1+l])
	val := binary.BigEndian.Uint64(tmp)
	consumed += uint64(l)

	*v = varUInt(val)
	return consumed, nil
}
