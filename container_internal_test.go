package edt

import (
	"reflect"
	"testing"
)

func Test_Container_Serialize_Length(t *testing.T) {
	for _, td := range SEDPTests {
		sekp, sedp, cnt, _ := newContainerFromTestData(td, t)

		b := cnt.Serialize()
		expectedLen := len(ContainerMagicNumber) +
			len(sekp.Serialize()) +
			len(sedp.Serialize())

		if len(b) != expectedLen {
			t.Error(
				"For", "Container.Serialize()",
				"expected length", expectedLen,
				"got", len(b),
			)
		}

	}
}

func Test_Container_SelfParse(t *testing.T) {
	for _, td := range SEDPTests {
		_, _, origcnt, _ := newContainerFromTestData(td, t)

		b := origcnt.Serialize()

		var cnt Container
		n, err := cnt.Parse(b)
		if err != nil {
			t.Error(
				"For Container.Parse(", b, ")",
				"got error", err,
			)
		}
		if n != uint64(len(b)) {
			t.Error(
				"For Container.Parse(", b, ")",
				"expected consumed bytes", len(b),
				"got", n,
			)
		}

		eq := reflect.DeepEqual(origcnt, cnt)
		if eq != true {
			t.Error(
				"For", "Container.Parse(Container.Serialize())",
				"expected", origcnt,
				"got", cnt,
			)
		}
	}
}

func Test_Container_Parse(t *testing.T) {
	for _, td := range SEDPTests {
		sekp, sedp, _ := newSEDPFromTestData(td, t)

		b := make([]byte, 0, 100)
		b = append(b, ContainerMagicNumber...)
		b = append(b, sekp.Serialize()...)
		b = append(b, sedp.Serialize()...)

		var cnt Container
		n, err := cnt.Parse(b)
		if err != nil {
			t.Error(
				"For Container.Parse(", b, ")",
				"got error", err,
			)
		}
		if n != uint64(len(b)) {
			t.Error(
				"For Container.Parse(", b, ")",
				"expected consumed bytes", len(b),
				"got", n,
			)
		}
	}
}

func Test_Container_Open(t *testing.T) {
	for _, td := range SEDPTests {
		_, _, cnt, _ := newContainerFromTestData(td, t)

		decrypted, err := cnt.Open(td.Passphrase)
		if err != nil {
			t.Error(
				"For", "Container.Open(", td.Passphrase, ")",
				"got error", err,
			)
		}
		if string(decrypted) != td.Plaintext {
			t.Error(
				"For", "Container.Open(", td.Passphrase, ")",
				"expected", td.Plaintext,
				"got", decrypted,
			)
		}
	}
}
