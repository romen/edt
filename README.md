[![GoDoc](https://godoc.org/bitbucket.org/romen/edt?status.svg)](https://godoc.org/bitbucket.org/romen/edt)

# EDT - Encryption/Decryption Tool

`edt` is a (simple) tool for encrypting and decrypting files

`edt` is also a Go package to handle the `edt` binary format or programmatically use its functionality.

Actually, the `edt` command just uses the `edt` package to provide a command line tool.

## Features

The main purpose of `edt` is performing Authenticated Encryption with Associated Data (**AEAD**) to simultaneously provide *confidentiality*, *integrity* and *authenticity*.

`edt` is ready to support different AEAD schemes, but currently, the only implemented ones are based on the **Galois/Counter Mode** on top of **AES**.

```
➜  edt --list-aead
Supported AEAD Modes:
        GCM_AES128
        GCM_AES256
```

To account for the different requirements of current and future AEAD modes, and to increase the resistance against dictionary/brute-force attacks, the passphrase provided for encryption/decryption is actually processed through a Key Derivation Function (**KDF**) to generate the actual key material.

`edt` is ready to support different KDF schemes, but currently, the only implemented one is based on **PBKDF2** (Password-Based Key Derivation Function 2, [RFC 2898](https://tools.ietf.org/html/rfc2898)) using a random salt and a hash function. The current version only supports **SHA3-512** as the underlying hash function.

```
➜  edt --list-kdf
Supported KDF Modes:
        PBKDF2_SHA3_512
```


## Build, Install & Run
```sh
➜  cd ${GOPATH} && go get bitbucket.org/romen/edt/cmd/edt
```

### Test
```sh
➜  cd ${GOPATH}/src/bitbucket.org/romen/edt/ && go test -v -cover
=== RUN   Test_Container_Serialize_Length
--- PASS: Test_Container_Serialize_Length (1.01s)
=== RUN   Test_Container_SelfParse
--- PASS: Test_Container_SelfParse (1.01s)
=== RUN   Test_Container_Parse
--- PASS: Test_Container_Parse (1.09s)
=== RUN   Test_Container_Open
--- PASS: Test_Container_Open (1.71s)
=== RUN   Test_opaquePacket_Parse_from_SEKP
--- PASS: Test_opaquePacket_Parse_from_SEKP (0.00s)
=== RUN   Test_opaquePacket_Parse_from_SEDP
--- PASS: Test_opaquePacket_Parse_from_SEDP (1.06s)
=== RUN   Test_ParsePacket_SEKP
--- PASS: Test_ParsePacket_SEKP (0.00s)
=== RUN   Test_ParsePacket_SEDP
--- PASS: Test_ParsePacket_SEDP (1.04s)
=== RUN   Test_SEDP_SelfOpen
--- PASS: Test_SEDP_SelfOpen (2.11s)
PASS
coverage: 75.7% of statements
ok      bitbucket.org/romen/edt 9.033s

```


### Run

#### Usage
```
➜  edt -h
Usage: edt [-eh] [--aead <AEADMode>] [--kdf <KDFMode>] [--list-aead] [--list-kdf] <file>
     --aead=<AEADMode>
                  Set the AEAD mode to be used for encryption.
                  (default: GCM_AES128)
 -e, --encrypt    Encrypt mode
 -h, --help       Display this help
     --kdf=<KDFMode>
                  Set the key derivation function mode to be used for
                  encryption. (default: PBKDF2_SHA3_512)
     --list-aead  List supported AEAD Modes
     --list-kdf   List supported KDF Modes
```

#### Encrypting a file (redirecting stdout)
```sh
➜  echo 'Hello World!!! (Psst! This is a secret!!!)' > /tmp/hello.txt
➜  edt -e /tmp/hello.txt > /tmp/hello.txt.edt
Passphrase: <fancy passphrase>
```

#### Decrypting a file (to stdout)
```sh
➜  edt /tmp/hello.txt.edt
Passphrase: <right passphrase>
Hello World!!! (Psst! This is a secret!!!)
➜  edt /tmp/hello.txt.edt
Passphrase: <wrong passphrase>
Error: cipher: message authentication failed
```

## `edt` file format

Each `edt` file contains a single `Container`: a `Container` is a sequence of `Packets` and its binary representation is given by a fixed octet sequence `0xed 0x45 0x44 0x54` followed by the binary representation of each contained `Packet`.

### Packets and Packet Headers

Each `Packet` starts with a variable-length `packetHeader`:
* an `HLength` octet stating the length in bytes of the `packetHeader`
* a `Version` octet stating the version of the `packetHeader`
* a `Tag` octet stating the type of the `Packet`
* a variable-length `BodyLength` field stating the length of the `Body` of the `Packet`:
  * the first octet represents the length of the rest of the field ( between 1 and 8 octets)
  * a number of octets containing the actual length value (in `BigEndian` notation)

The `packetHeader` is followed by the `Body` of the `Packet`, whose format and contents depend on the `Packet Type`.

### Packet Types

#### Symmetric Encryption Key Packet (`SEKP`)

Its `Body` contains 4 fields:
* `KDFMode`, a 2-octets field, representing the selected **KDF** mode
* `Iterations`, an octet representing the number of iterations for the KDF (the value of this field multiplied by 1024)
* `SaltLength`, an octet representing the length of the `Salt` field
* `Salt`, a variable-length random octet string used during **KDF**

Further details are documented in `packet_sek.go`.

#### Symmetric Encrypted Data Packet (`SEDP`)

Its `Body` starts with an `sedpPreamble`, containing the following fields:
* `AEADMode`, a 2-octets field, representing the selected **AEAD** mode
* `NonceLength`, an octet representing the length of the `Nonce` field
* `Nonce`, a variable-length random octet string used during **AEAD**

The rest of the `Body` contains the field `CT`, an octet string representing the encrypted Ciphertext and the authentication Tag, computed over the plaintext and associated data (comprising the binary representation of the associated `SEKP` and the `packetHeader` and `sedpPreamble` portions of this `Packet`).

Further details are documented in `packet_sed.go`.

### Example

This is the content of the previously generated file:
```sh
➜  hexdump -C /tmp/hello.txt.edt
00000000  ed 45 44 54 05 01 01 01  0c 00 01 16 08 3d 16 af  |.EDT.........=..|
00000010  a1 93 19 f4 8b 05 01 02  01 4a 80 01 0c ab c1 0a  |.........J......|
00000020  51 ef 0d 39 92 cc a8 13  84 fb 35 97 60 86 13 ea  |Q..9......5.`...|
00000030  9c cc cd 73 cb c3 ef 47  cd 6f 64 8d 86 35 df 51  |...s...G.od..5.Q|
00000040  bc 7e 8c 8f 8b 3e 65 d8  7b 53 6e 78 45 a9 5d d5  |.~...>e.{SnxE.].|
00000050  77 98 04 25 e0 37 2a f8  a2 9b 97 6a 79 ec 57 e7  |w..%.7*....jy.W.|
00000060  c7 72 b8 4b                                       |.r.K|
00000064
```

### Limitations

The current alpha version of `edt` expects a `Container` to contain exactly one `SEKP` followed by an `SEDP`: more flexibility for different use cases (e.g. supporting Hybrid Encryption, mixing Symmetric Keys and Asymmetric Keys ) is possible and definitely on the `TODO` list for further development of this tool.

## TODO

- [ ] support Hybrid Encryption
- [ ] implement more AEAD modes (e.g. Offset Codebook Mode (**[OCB](http://web.cs.ucdavis.edu/~rogaway/ocb/)**, \[[RFC 7253](https://tools.ietf.org/html/rfc7253)\]) and Sponge/Duplex based (see  \[[1](http://sponge.noekeon.org/SpongeDuplex.pdf),[2](http://keccak.noekeon.org/KeccakDIAC2012.pdf)\])
- [ ] implement more KDF modes
- [ ] improve the API to use Golang `Reader` and `Writer` interfaces and avoid buffering (i.e. don't use the current version to encrypt/decrypt big files!!!!)
- [ ] improve the quality of code: this project was meant to learn the Go language, so many things are not as *beautifully coded* as they might be with more experience and understanding of further details of the language syntax, common paradigms, idiomatic conventions and the underlying software architecture rationales.
- [ ] improve code documentation: I still have to understand how to produce `godoc` documentation and how to properly document in the code.
- [ ] licensing?
