package edt

import (
	"encoding/binary"
)

// An NID is a 16 bits octet string uniquely associated with a particular entity
// of this package.
type NID uint16

// A PTAG is a 8-bit identifier uniquely associated with a Package type.
type PTAG uint8

// KDFMode NIDs
const (
	NID_undef NID = iota

	NID_PBKDF2_SHA3_512
)

// AEAD NIDs
const (
	NID_GCM_AES128 NID = 1 + iota + 1<<15
	NID_GCM_AES256
)

const (
	// PTAG_undef is associated with the internal representation of an
	// unsupported Packet type
	PTAG_undef PTAG = iota

	// PTAGSymmetricEncryptionKey uniquely identifies a packet as a SEKP
	PTAGSymmetricEncryptionKey

	// PTAGSymmetricEncryptedData uniquely identifies a packet as a SEDP
	PTAGSymmetricEncryptedData
)

// Serialize returns the binary representation of this NID
func (nid NID) Serialize() []byte {
	out := make([]byte, 2)
	binary.BigEndian.PutUint16(out, uint16(nid))
	return out
}

// ParseNID reads an octet string from an input buffer and validates it as
// a NID, returning the number of bytes read and the NID value
func ParseNID(in []byte) (n uint64, nid NID) {
	if len(in) < 2 {
		return 0, NID_undef
	}
	nid = NID(binary.BigEndian.Uint16(in[:2]))
	return 2, nid
}

// Serialize returns the binary representation of this PTAG
func (ptag PTAG) Serialize() []byte {
	return []byte{uint8(ptag)}
}
