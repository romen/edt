package edt

import (
	"testing"
)

/*
	sekp, err := newSEKPFromTestData(td, t)
	sekp, sedp, err := newSEDPFromTestData(td, t)
	sekp, sedp, cnt, err := newContainerFromTestData(td, t)
*/

func newSEKPFromTestData(td newSEKPDataType, t *testing.T) (sekp SymmetricEncryptionKeyPacket, err error) {
	sekp, err = NewSEKPacketFromNID(td.KDFnid)
	if err != nil {
		t.Error(
			"For newSEKPacket(", td.KDFnid, ")",
			"got error", err,
		)
	}
	return
}

func newSEDPFromTestData(td newSEKDDataType, t *testing.T) (sekp SymmetricEncryptionKeyPacket, sedp SymmetricEncryptedDataPacket, err error) {
	sekp, err = NewSEKPacketFromNID(td.KDFnid)
	if err != nil {
		t.Error(
			"For", "NewSEKPacketFromNID(", td.KDFnid, ")",
			"got error", err,
		)
		return
	}

	sedp, err = NewSEDPacketWithSEKP(td.Passphrase, []byte(td.Plaintext), td.AEADnid, &sekp)
	if err != nil {
		t.Error(
			"For",
			"NewSEDPacketWithSEKP(", td.Passphrase, []byte(td.Plaintext), td.AEADnid, &sekp, ")",
			"got error", err,
		)
	}

	return
}

func newContainerFromTestData(td newSEKDDataType, t *testing.T) (sekp SymmetricEncryptionKeyPacket, sedp SymmetricEncryptedDataPacket, container Container, err error) {
	sekp, sedp, err = newSEDPFromTestData(td, t)

	container.AppendPkt(&sekp)
	container.AppendPkt(&sedp)

	return
}
