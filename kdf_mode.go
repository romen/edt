package edt

import (
	"fmt"
)

// The KDFMode interface implements a key derivation function Key() with the
// following parameters:
//
// Input:
//  *	passphrase	(UTF-8 string, will be normalized and converted into []byte)
//  *	salt		(random byte sequence >= 8bytes to use as salt)
//  *	iter		(number of iterations, the higer the more cumbersome/resistant against brute force)
//  *	keyLen		(desired output key length)
//
// Output:
//  *	key []byte
type KDFMode interface {
	// derive a key
	Key(passphrase string, salt []byte, iter, keyLen int) []byte

	// returns the NID for this KDFMode
	GetNID() NID

	// RIterations() returns a randomized appropriate number of iterations to
	// perform key derivation with this KDFMode
	RIterations() int

	// RSalt returns an appropriate random salt octet string
	RSalt() []byte
}

// NewKDFMode is a factory-like constructor to get a KDFMode instance given a NID
func NewKDFMode(nid NID) (KDFMode, error) {
	switch nid {
	case NID_PBKDF2_SHA3_512:
		return newPBKDF2_SHA3_512(), nil
	default:
		return nil, fmt.Errorf("unsupported KDFModeNID: %#v", nid)
	}
}
