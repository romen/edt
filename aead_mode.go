package edt

import (
	"crypto/cipher"
	"fmt"
)

// An AEADMode is an abstract object used to produce instances of a specific AEAD interface
type AEADMode interface {
	// GetNID() returns the NID for this AEADMode
	GetNID() NID

	// KeyLength() returns the required length for the key to be fed to the AEAD() method
	KeyLength() int

	// AEAD(), given a KeyLength()-long key, returns an instance of an
	// AEAD interface ( https://golang.org/pkg/crypto/cipher/#AEAD )
	// implementing the AEAD described by this AEADMode
	AEAD(key []byte) (cipher.AEAD, error)
}

// NewAEADMode is a factory-like constructor to get an AEADMode instance given an NID
func NewAEADMode(nid NID) (AEADMode, error) {
	switch nid {
	case NID_GCM_AES128:
		return newGCM_AES128(), nil
	case NID_GCM_AES256:
		return newGCM_AES256(), nil
	default:
		return nil, fmt.Errorf("Unsupported AEADModeNID: %#v", nid)
	}
}
