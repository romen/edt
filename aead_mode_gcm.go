package edt

import (
	"crypto/aes"
	"crypto/cipher"
	"fmt"
)

// An AEADMode implementation that supports GCM with AES128 or AES256.
// The actual block cipher is selected through the length of the supplied key,
// the only data stored in baseGCM is the associated NID to provide
// appropriate values for GetNID() and KeyLength()
type baseGCM struct {
	nid NID
}

// KeyLength() returns the required length for the key to be fed to the AEAD() method
// 		when AEAD() is given a 16 byte key AES128-GCM is instantiated, when
// 		fed a 32-byte key AES256-GCM is instantiated: the return value of
// 		KeyLength() is selected according to the nid stored internally to
// 		select between AES128/AES256
func (b *baseGCM) KeyLength() int {
	switch b.nid {
	case NID_GCM_AES128:
		return 16
	case NID_GCM_AES256:
		return 32
	default: // this should never happen, treat as a fatal error
		panic("baseGCM.KeyLength(): unsupported NID")
	}
}

// GetNID() returns the NID for this AEADMode
func (b *baseGCM) GetNID() NID {
	return b.nid
}

// AEAD(), given a KeyLength()-long key, returns an instance of an AEAD interface
//		this method supports GCM with AES128 or AES256: the actual block
//		cipher implementation is selected by the key length
func (b *baseGCM) AEAD(key []byte) (cipher.AEAD, error) {
	if len(key) != b.KeyLength() {
		return nil, fmt.Errorf("The input key must be exactly %d bytes long", b.KeyLength())
	}
	aes, err := aes.NewCipher(key)
	if err != nil {
		return nil, fmt.Errorf("NewCipher() returned %s", err.Error())
	}

	return cipher.NewGCM(aes)
}

// newGCM_AES128() is a private constructor for a AEADMode based on AES128-GCM
func newGCM_AES128() AEADMode {
	return &baseGCM{NID_GCM_AES128}
}

// newGCM_AES256() is a private constructor for a AEADMode based on AES256-GCM
func newGCM_AES256() AEADMode {
	return &baseGCM{NID_GCM_AES256}
}
