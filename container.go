package edt

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
)

// A Container contains a sequence of Packets.
type Container struct {
	pkts []Packet
}

// AppendPkt append a Packet to the Container
func (c *Container) AppendPkt(p Packet) {
	c.pkts = append(c.pkts, p)
}

// Seal sets a Container to contain the sealed (authenticated encryption)
// representation of the given plaintext under the specified AEADMode,
// deriving the key material using the KDFMode specified by kdfnid and the
// specified passphrase
func (c *Container) Seal(
	passphrase string,
	plaintext io.Reader,
	kdfnid, aeadnid NID,
) (n int, err error) {
	sekp, err := NewSEKPacketFromNID(kdfnid)
	if err != nil {
		return
	}
	c.AppendPkt(&sekp)

	buffer, err := ioutil.ReadAll(plaintext)
	if err != nil {
		return
	}
	sedp, err := NewSEDPacketWithSEKP(passphrase, buffer, aeadnid, &sekp)
	if err != nil {
		return
	}
	c.AppendPkt(&sedp)

	return
}

// Open decrypts and authenticates the contents of the container under the
// given passphrase
func (c *Container) Open(passphrase string) (plaintext []byte, err error) {
	// BUG(romen): currenly we assume that the continer MUST contain exactly a
	// SEK Packet followed by a SED Packet

	// check the number of packets, should be exactly 2
	if len(c.pkts) != 2 {
		err = fmt.Errorf("Unsupported Container: should contain exactly 2 Packets")
		return
	}

	// try Type assertions to specialized SEKP and SEDP
	sekp, firstIsSEKP := (c.pkts[0]).(*SymmetricEncryptionKeyPacket)
	sedp, secondIsSEDP := (c.pkts[1]).(*SymmetricEncryptedDataPacket)
	if !firstIsSEKP || !secondIsSEDP {
		err = fmt.Errorf("Unsupported Container: should contain {SEKP,SEDP}")
		return
	}

	// The Container seems valid, open the SED Packet using the passphrase and
	// the SEK Packet
	return sedp.Open(passphrase, sekp)
}

// ContainerMagicNumber contains the fixed octet string at the beginning of
// every valid Container
var ContainerMagicNumber = []byte{0xED, 0x45, 0x44, 0x54}

// Serialize returns a binary representation of the Container
func (c *Container) Serialize() []byte {
	var buff bytes.Buffer
	buff.Write(ContainerMagicNumber)
	for _, p := range c.pkts {
		buff.Write(p.Serialize())
	}
	return buff.Bytes()
}

// Parse parses an octet string into a Container, asserting the validity of
// the representation
func (c *Container) Parse(in []byte) (n uint64, err error) {
	// A valid Container must start with CONTAINER_MAGIC_NUMBER octet sequence
	if len(in) < len(ContainerMagicNumber) {
		err = fmt.Errorf("Buffer too short")
		return
	}
	magic := in[:len(ContainerMagicNumber)]
	if !bytes.Equal(magic, ContainerMagicNumber) {
		err = fmt.Errorf("Invalid Container format")
		return
	}
	n = uint64(len(ContainerMagicNumber))

	// A valid Container then is followed by a sequnce of valid Packets
	for len(in[n:]) > 0 {
		var r uint64
		var p Packet

		// parse the next Packet from the buffer
		r, p, err = ParsePacket(in[n:])
		if err != nil {
			return
		}
		// append the parsed Packet to the internal slice
		c.AppendPkt(p)

		// update the "consumed bytes" counter
		n += r
	}

	return
}
