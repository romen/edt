package edt

import "fmt"

// The Packet interface represent any Packet, simply mandating a Serialize and
// a Parse method.
type Packet interface {
	// Serialize returns an octet string containing a valid binary
	// representation of this Packet, starting with a valid Packet Header.
	Serialize() []byte

	// Parse reads an octet string from an input buffer and validates it as
	// a Packet of the type specified by the actual implementation of this
	// interface, updating its internal state to reflect the read data.
	//
	// It returns the number of correctly read bytes or an error.
	Parse(in []byte) (n uint64, err error)
}

// An opaquePacket is an actual implementation of the Packet Interface capable
// of representing an unknown valid packet, containing a valid Packet Header
// followed by some opaque unkown body.
//
type opaquePacket struct {
	packetHeader
	rawPacket []byte
	header    []byte
	body      []byte
}

// Serialize returns an octet string containing the binary representation of
// this opaquePacket.
func (p *opaquePacket) Serialize() []byte {
	return p.rawPacket
}

// Parse reads an octet string from an input buffer and validates it as an opaquePacket containing a valid packetHeader followed by an opaque body represented as on octet string of the specified in its Packet Header.
//
// Upon correct validation of the Packet Header, it updates the internal state of this opaquePacket struct to reflect the read data.
//
// It returns the number of correctly read bytes or an error.
func (p *opaquePacket) Parse(in []byte) (n uint64, err error) {
	n, err = p.packetHeader.parse(in)
	if err != nil || n != uint64(p.HLength) {
		if err == nil {
			err = fmt.Errorf("Malformed Packet Header")
		}
		return 0, err
	}

	plength := uint64(p.HLength) + uint64(p.BodyLength)
	p.rawPacket = append([]byte(nil), in[:plength]...) // copy relevant part of in
	p.header = p.rawPacket[:n]
	p.body = p.rawPacket[n:]

	return plength, nil
}

// ParsePacket reads the given input buffer, validates its header and returns an
// object implementing the Packet interface, either as a specialized in-memory
// representation of a Package typed according to the Tag packetHeader field or
// as an opaquePacket.
func ParsePacket(in []byte) (n uint64, p Packet, err error) {
	var op opaquePacket
	n, err = op.Parse(in)
	if err != nil {
		return
	}

	switch op.Tag {
	case PTAGSymmetricEncryptionKey:
		var r uint64
		var sekp SymmetricEncryptionKeyPacket
		r, err = sekp.Parse(op.Serialize())
		if err == nil && r == n {
			return n, &sekp, nil
		}
		if err == nil {
			err = fmt.Errorf("Malformed SymmetricEncryptionKey Packet")
		}
	case PTAGSymmetricEncryptedData:
		var r uint64
		var sedp SymmetricEncryptedDataPacket
		r, err = sedp.Parse(op.Serialize())
		if err == nil && r == n {
			return n, &sedp, nil
		}
		if err == nil {
			err = fmt.Errorf("Malformed SymmetricEncryptedData Packet")
		}
	case PTAG_undef:
		err = fmt.Errorf("Invalid Packet Tag")
		return 0, nil, err
	}
	return n, &op, err
}
