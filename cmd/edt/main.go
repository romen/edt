package main

import (
	"bitbucket.org/romen/edt"
	"fmt"
	"github.com/pborman/getopt/v2"
	"golang.org/x/crypto/ssh/terminal"
	"io"
	"io/ioutil"
	"os"
)

const (
	DEFAULT_KDFMODE_STR  = "PBKDF2_SHA3_512"
	DEFAULT_AEADMODE_STR = "GCM_AES128"
)

var ( // Command line option pointers
	helpFlag     = getopt.BoolLong("help", 'h', "Display this help")
	encryptFlag  = getopt.BoolLong("encrypt", 'e', "Encrypt mode")
	listKDFFlag  = getopt.BoolLong("list-kdf", 0, "List supported KDF Modes")
	listAEADFlag = getopt.BoolLong("list-aead", 0, "List supported AEAD Modes")
	kdfmodestr   = getopt.StringLong("kdf", 0, DEFAULT_KDFMODE_STR,
		fmt.Sprintf("Set the key derivation function mode to be used for encryption. (default:%c%s)", '\u00A0', DEFAULT_KDFMODE_STR),
		"<KDFMode>",
	)
	aeadmodestr = getopt.StringLong("aead", 0, DEFAULT_AEADMODE_STR,
		fmt.Sprintf("Set the AEAD mode to be used for encryption. (default:%c%s)", '\u00A0', DEFAULT_AEADMODE_STR),
		"<AEADMode>",
	)
)

var (
	KDFMap = map[string]edt.NID{
		"PBKDF2_SHA3_512": edt.NID_PBKDF2_SHA3_512,
	}
	AEADMap = map[string]edt.NID{
		"GCM_AES128": edt.NID_GCM_AES128,
		"GCM_AES256": edt.NID_GCM_AES256,
	}

	kdfnid  edt.NID
	aeadnid edt.NID
)

func init() {
	getopt.SetProgram("edt")
	getopt.SetParameters("<file>")

	getopt.Parse()

	if *helpFlag {
		usage()
	}

	if *listKDFFlag {
		fmt.Fprintf(os.Stderr, "Supported KDF Modes:\n")
		for k, _ := range KDFMap {
			fmt.Fprintf(os.Stderr, "\t%s\n", k)
		}
		os.Exit(1)
	}

	if *listAEADFlag {
		fmt.Fprintf(os.Stderr, "Supported AEAD Modes:\n")
		for k, _ := range AEADMap {
			fmt.Fprintf(os.Stderr, "\t%s\n", k)
		}
		os.Exit(1)
	}
}

func usage() {
	getopt.PrintUsage(os.Stderr)
	os.Exit(1)
}

func main() {
	args := getopt.Args()
	if len(args) != 1 {
		usage()
	}

	kdfnid = KDFMap[*kdfmodestr]
	aeadnid = AEADMap[*aeadmodestr]

	inputfile := args[0]

	input, err := os.Open(inputfile)
	check(err)

	if *encryptFlag {
		if kdfnid == edt.NID_undef {
			check(fmt.Errorf("Invalid KDF Mode"))
		}
		if aeadnid == edt.NID_undef {
			check(fmt.Errorf("Invalid AEAD Mode"))
		}
		encrypt(input, os.Stdout)
		return
	} else {
		if getopt.IsSet("kdf") {
			check(fmt.Errorf("Cannot specify KDF Mode for decryption"))
		}
		if getopt.IsSet("aead") {
			check(fmt.Errorf("Cannot specify AEAD Mode for decryption"))
		}
		decrypt(input, os.Stdout)
		return
	}
}

func getPassphrase() (passphrase string, err error) {
	stdinFD := int(os.Stdin.Fd())

	oldState, err := terminal.MakeRaw(stdinFD)
	if err != nil {
		return passphrase, err
	}
	defer terminal.Restore(stdinFD, oldState)

	nt := terminal.NewTerminal(os.Stdin, "> ")

	passphrase, err = nt.ReadPassword("Passphrase: ")
	if err != nil {
		return
	}

	return
}

func decrypt(input io.Reader, output io.Writer) {
	// TODO: change Container API to avoid buffering
	buffer, err := ioutil.ReadAll(input)
	check(err)

	var cnt edt.Container
	_, err = cnt.Parse(buffer)
	check(err)

	passphrase, err := getPassphrase()
	check(err)

	decrypted, err := cnt.Open(passphrase)
	check(err)

	output.Write(decrypted)
	return
}

func encrypt(input io.Reader, output io.Writer) {
	// TODO: change Controller.Seal() interface to use a Reader for the Passphrase
	passphrase, err := getPassphrase()
	check(err)

	var cnt edt.Container
	_, err = cnt.Seal(passphrase, input, kdfnid, aeadnid)
	check(err)

        _, err = output.Write(cnt.Serialize())
        check(err)

	return
}

func check(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error: %s\n", err)
		os.Exit(1)
	}
}
