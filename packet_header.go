package edt

import "fmt"

// phMinLength is a contant representing the minimum length of a packetHeader
const phMinLength = (1 /* HLength */ +
	1 /* Version */ +
	1 /* Tag */ +
	2 /* ph.BodyLength.Serialize() minimum size is 2 bytes */)

// A packetHeader struct represents the standard Packet Header part of every
// valid Packet.
type packetHeader struct {
	HLength    uint8   // packetHeader Length = 1 (HLength) + 1 (Version) + 1 (Tag) + x (PLength)
	Version    uint8   // packetHeader Version = 0x1
	Tag        PTAG    // Packet type
	BodyLength varUInt // Packet body lenght (Packet length excluding header)
}

// adjustHLength updates the internal status of this packetHeader so that the
// HLength is adjusted taking in consideration the actual size of the
// variable-length BodyLength field.
func (ph *packetHeader) adjustHLength() {
	ph.HLength = 1 /* HLength */ +
		1 /* Version */ +
		1 /* Tag */ +
		uint8(len(ph.BodyLength.Serialize()))
}

// serialize returns an octet string containing the binary representation of
// this packetHeader.
func (ph *packetHeader) serialize() []byte {
	out := make([]byte, 0, ph.HLength)
	out = append(out, byte(ph.HLength))
	out = append(out, byte(ph.Version))
	out = append(out, ph.Tag.Serialize()...)
	out = append(out, ph.BodyLength.Serialize()...)

	return out
}

// parse reads an octet string from the input buffer and validates it as
// a packetHeader.
//
// Upon successfull validation, the internal status of this packetHeader object
// are updated to reflect the read data.
//
// It returns the number of bytes correctly read or an error.
func (ph *packetHeader) parse(in []byte) (n uint64, err error) {
	n = 0
	hlen := uint64(in[0])
	if hlen < phMinLength || hlen > uint64(len(in)) {
		err = fmt.Errorf("Wrong HLength")
		return
	}

	version := in[1]
	if version != 0x1 {
		err = fmt.Errorf("Unsupported packetHeader version: %#x", version)
		return
	}

	// We suport any Tag != PTAG_undef here, let the caller handle unknown tags
	tag := in[2]
	if PTAG(tag) == PTAG_undef {
		err = fmt.Errorf("Unsupported packetHeader tag: %#x", tag)
		return
	}

	var blen varUInt
	blenlen, err := blen.Parse(in[3:])
	if err != nil || 3+blenlen != hlen {
		if err == nil {
			err = fmt.Errorf("Malformed Packet Header, wrong BodyLength: %#x", in[3:blenlen])
		}
		return
	}

	ph.HLength = uint8(hlen)
	ph.Version = version
	ph.Tag = PTAG(tag)
	ph.BodyLength = blen

	return hlen, nil
}
