package edt

import (
	"bytes"
	"crypto/rand"
	"fmt"
	"golang.org/x/crypto/sha3"
)

// A SymmetricEncryptedDataPacket represents a Symmetric Encrypted Data Packet
// (SEDP).
//
// Like any other packet it is composed of the standard Packet Header, while its
// body is composed of a a Preamble (sedpPreamble) and an octet string, CT,
// representing the encrypted Ciphertext and the authentication Tag, computed
// over the plaintext and associated data (comprising the binary representation
// of the associated SEKP concatenated with the packetHeader and sedpPreamble
// portions of the SED Packet).
type SymmetricEncryptedDataPacket struct {
	packetHeader
	sedpPreamble

	// concatenated Encrypted Ciphertext (C) + Authentication Tag (T)
	CT []byte
}

// A sedpPreamble represents the Pramble part of a SEDP Body, containing the
// following fields:
type sedpPreamble struct {
	// A 2-octets field, containind the NID for the selected AEAD mode
	AEADMode NID

	// An octet representing the length of the following Nonce field
	NonceLength uint8

	// A variable-length octet string containing a random nonce
	Nonce []byte
}

// Length returns the length of this sedpPreamble
func (pr *sedpPreamble) length() int {
	return 3 + int(pr.NonceLength)
}

// Serialize returns the serialized octet representation of this sedpPreamble
func (pr *sedpPreamble) serialize() []byte {
	out := make([]byte, 0, pr.length())
	out = append(out, pr.AEADMode.Serialize()...)
	out = append(out, byte(pr.NonceLength))
	out = append(out, pr.Nonce...)
	return out
}

// Parse reads and validates an octet string representation of a sedpPreamble and
// updates the internal state of this sedpPreamble struct to reflect the parsed
// contents. Returns the number of bytes read or an error.
func (pr *sedpPreamble) parse(in []byte) (n uint64, err error) {
	// Parse the AEADMode NID fields
	r, aeadnid := ParseNID(in)
	if r != 2 || aeadnid == NID_undef {
		err = fmt.Errorf("Malformed AEADMode NID")
		return 0, err
	}
	n = r

	nonceLength := uint8(in[n])
	n++
	var nonce []byte

	// validate the read nonceLength field
	if int(nonceLength) > len(in[n:]) {
		err = fmt.Errorf("Invalid NonceLength field")
		return 0, err
	}

	// make a new copy of the nonce slice
	nonce = append(nonce, in[n:n+uint64(nonceLength)]...)
	n += uint64(nonceLength)

	// once the sedpPreamble is validated, update the state of this
	// sedpPreamble
	pr.AEADMode = aeadnid
	pr.NonceLength = nonceLength
	pr.Nonce = nonce

	return n, nil
}

// Serialize returns the serialized binary representation of this SEDP
func (sedp *SymmetricEncryptedDataPacket) Serialize() []byte {
	out := make([]byte, 0, uint64(sedp.HLength)+uint64(sedp.BodyLength))
	out = append(out, sedp.packetHeader.serialize()...)
	out = append(out, sedp.sedpPreamble.serialize()...)
	out = append(out, sedp.CT...)
	return out
}

// Parse reads an input octet string and validates it as the binary
// representation of a SED Packet.
//
// Upon successful validation it updates the internal status of this
// SymmetricEncryptedDataPacket structure to reflect the read data.
//
// It returns the number of bytes successfully read from the input buffer or an
// error.
func (sedp *SymmetricEncryptedDataPacket) Parse(in []byte) (n uint64, err error) {
	var r uint64
	n, err = sedp.packetHeader.parse(in)
	if err != nil {
		return 0, err
	}

	r, err = sedp.sedpPreamble.parse(in[n:])
	if err != nil {
		return 0, err
	}
	n += r

	// make a new copy of the ct slice
	var ct []byte
	ct = append(ct, in[n:uint64(sedp.HLength)+uint64(sedp.BodyLength)]...)
	n += uint64(len(ct))

	sedp.CT = ct

	return
}

// NewSEDPacketWithSEKP is a convenience function, using NewSEDPacket and
// automating steps to derive the key material with the parameters from a SEK
// Package
func NewSEDPacketWithSEKP(
	passphrase string, // arbitrary UTF-8 passphrase
	plaintext []byte, // plaintext as a []byte slice
	aeadnid NID, // NID for the selected AEADMode
	sekp *SymmetricEncryptionKeyPacket, // reference to a valid SEK Packet
) (sedp SymmetricEncryptedDataPacket, err error) {
	// retrieve the AEADMode from the NID
	aeadmode, err := NewAEADMode(aeadnid)
	if err != nil {
		return
	}

	// derive a key suitable for AEAD using the SEK Package parameters
	key, err := sekp.Key(passphrase, aeadmode.KeyLength())
	if err != nil {
		return
	}

	return NewSEDPacket(key, plaintext, sekp.Serialize(), aeadnid)

}

// NewSEDPacket automates the creation of a new Symmetric Encripted Data (SED)
// Packet
func NewSEDPacket(
	key []byte, // key material, its length MUST match the specific AEAD requirements
	plaintext []byte, // plaintext material
	// associated data to authenticate alongside encryption (fields of the SEDP will be appended to this
	associatedData []byte,
	aeadnid NID, // NID of the selected AEADMode
) (sedp SymmetricEncryptedDataPacket, err error) {
	sedp = SymmetricEncryptedDataPacket{
		packetHeader: packetHeader{
			Version: 0x1,
			Tag:     PTAGSymmetricEncryptedData,
		},
		sedpPreamble: sedpPreamble{
			AEADMode: aeadnid,
		},
	}

	aeadmode, err := NewAEADMode(aeadnid)
	if err != nil {
		return
	}

	// check that the received keylength matches the requirements
	if len(key) != aeadmode.KeyLength() {
		err = fmt.Errorf("Wrong key length: got %d, expected %d", len(key), aeadmode.KeyLength())
		return
	}

	// instantiate the actual AEAD interface with the derived key
	aead, err := aeadmode.AEAD(key)
	if err != nil {
		return
	}

	{ /* generate random nonce suitable for AEAD */
		nonceSize := aead.NonceSize()
		nonce := make([]byte, nonceSize)

		_, err := rand.Read(nonce)
		if err != nil { // this is a fatal error
			panic("Error generating random nonce: " + err.Error())
		}

		/* Poor PRNG entropy might lead to potential vulnerabilities:
		 * as a paranoid precaution we hash the generated randomness
		 * and the derived key through SHAKE256
		 */
		hash := sha3.NewShake256() // Use SHAKE256 to avoid unsafe PRNG failures
		hash.Write(nonce)
		hash.Write(key)
		hash.Read(nonce)

		sedp.Nonce = nonce

		if nonceSize >= 256 {
			return sedp, fmt.Errorf("Requested more than 255 bytes of nonce")
		}
		sedp.NonceLength = uint8(nonceSize)
	}

	ctlen := len(plaintext) + aead.Overhead() /* len(sedp.CT) */
	sedp.BodyLength = (2 + 1 + varUInt(sedp.NonceLength)) /* Preamble */ + varUInt(ctlen)
	sedp.adjustHLength()

	// concatenate fields of this package to the associated data to be authenticated:
	//		associatedData || this.packetHeader || this.sedpPreamble
	var adBuff bytes.Buffer
	adBuff.Write(associatedData)
	adBuff.Write(sedp.packetHeader.serialize())
	adBuff.Write(sedp.sedpPreamble.serialize())

	ciphertext := aead.Seal(nil, sedp.Nonce, plaintext, adBuff.Bytes())

	sedp.CT = ciphertext

	return sedp, nil
}

// GetAEADMode returns a new instance of the AEADMode specified by this SEDP
func (sedp *SymmetricEncryptedDataPacket) GetAEADMode() (AEADMode, error) {
	return NewAEADMode(sedp.AEADMode)
}

// Open decrypts and authenticates ciphertext, authenticates the
// additional data and, if successful, returns the resulting plaintext
// as a byte slice.
//
// The parameters for deriving the key from the passpharase are obtained from
// the given SEK Packet, as well as the parameters for the actual authnticated
// encryption and the associated data for verifying the authentication tag.
func (sedp *SymmetricEncryptedDataPacket) Open(
	passphrase string,
	sekp *SymmetricEncryptionKeyPacket,
) (plaintext []byte, err error) {
	plaintext = nil
	// retrieve the AEADMode from this SED Package
	aeadmode, err := sedp.GetAEADMode()
	if err != nil {
		return
	}
	// retrieve the KDFMode from the SEK Package
	kdfmode, err := sekp.GetKDFMode()
	if err != nil {
		return
	}
	// derive a key suitable for AEAS using the SEK Package parameters
	key := kdfmode.Key(passphrase, sekp.Salt, int(sekp.Iterations)<<10, aeadmode.KeyLength())

	// instantiate the actual AEAD interface with the derived key
	aead, err := aeadmode.AEAD(key)
	if err != nil {
		return
	}

	// concatenate associated data to be authenticated:
	//		SEK Package + this.packetHeader + this.sedpPreamble
	var adBuff bytes.Buffer
	adBuff.Write(sekp.Serialize())
	adBuff.Write(sedp.packetHeader.serialize())
	adBuff.Write(sedp.sedpPreamble.serialize())

	// Decrypt and authenticate ciphertext
	return aead.Open(nil, sedp.Nonce, sedp.CT, adBuff.Bytes())
}
