package edt

import (
	"fmt"
)

// A SymmetricEncryptionKeyPacket struct represents a Symmetric Encryption Key
// Packages (SEKP).
//
// Like any other package it is composed of the standard Packet Header, while
// its body contains the parameters required for deriving the actual key
// material used for symmetric encryption from a passphrase:
type SymmetricEncryptionKeyPacket struct {
	packetHeader

	// A 2-octets field, containind the NID for the selected KDF mode
	KDFMode NID

	// An octet representing the number of iterations for the KDF algorithm
	// ( the KDF performs Iterations times 1024)
	Iterations uint8

	// An octet representing the length of the following Salt field
	SaltLength uint8

	// A variable length octet string containing a random salt used in the
	// KDF function
	Salt []byte
}

// Serialize returns the serialized binary representation of this SEKP
func (p *SymmetricEncryptionKeyPacket) Serialize() []byte {
	out := make([]byte, 0, uint64(p.HLength)+uint64(p.BodyLength))
	out = append(out, p.packetHeader.serialize()...)
	out = append(out, p.KDFMode.Serialize()...)
	out = append(out, p.Iterations)
	out = append(out, p.SaltLength)
	out = append(out, p.Salt...)

	return out
}

// Parse reads an input octet string and validates it as the binary
// representation of a SEK Packet.
//
// Upon successful validation it updates the internal status of this
// SymmetricEncryptionKeyPacket structure to reflect the read data.
//
// It returns the number of bytes successfully read from the input buffer or an
// error.
func (p *SymmetricEncryptionKeyPacket) Parse(in []byte) (n uint64, err error) {
	var r uint64
	n, err = p.packetHeader.parse(in)
	if err != nil {
		return 0, err
	}

	r, kdfnid := ParseNID(in[n:])
	if r != 2 || kdfnid == NID_undef {
		err = fmt.Errorf("Malformed KDFMode NID")
		return 0, err
	}
	n += r

	iter := uint8(in[n])
	n++

	saltlen := uint8(in[n])
	n++
	var salt []byte
	if int(saltlen) > len(in[n:]) {
		err = fmt.Errorf("Invalid SaltLength field")
		return 0, err
	}
	// make a new copy of the salt slice
	salt = append(salt, in[n:n+uint64(saltlen)]...)
	n += uint64(saltlen)

	p.KDFMode = kdfnid
	p.Iterations = iter
	p.SaltLength = saltlen
	p.Salt = salt

	return
}

// GetKDFMode returns an instance of the KDFMode specified in this SEKP or an
// error in case the specified SEKP is not supported.
func (p *SymmetricEncryptionKeyPacket) GetKDFMode() (KDFMode, error) {
	return NewKDFMode(p.KDFMode)
}

// Key derives a keyLen-bytes key from the given passphrase, using the KDF
// settings specified in the SymmetricEncryptionKeyPacket
func (p *SymmetricEncryptionKeyPacket) Key(
	passphrase string,
	keyLen int,
) (key []byte, err error) {
	key = nil
	kdfmode, err := p.GetKDFMode()
	if err != nil {
		return
	}
	key = kdfmode.Key(passphrase, p.Salt, int(p.Iterations)<<10, keyLen)
	return
}

// NewSEKPacketFromNID creates a Symmetric Encryption Key Packet, retrieving
// randomized parameters from the KDFMode corresponding to the given KDFnid
func NewSEKPacketFromNID(KDFnid NID) (sekp SymmetricEncryptionKeyPacket, err error) {
	kdfmode, err := NewKDFMode(KDFnid)
	if err != nil {
		return
	}
	return NewSEKPacket(kdfmode, kdfmode.RIterations(), kdfmode.RSalt())
}

// NewSEKPacket creates a Symmetric Encryption Key Packet, using the provided
// KDFMode, iterations count and salt
//
// Input:
//		KDFMode			(a valid KDFMode object)
//		iterations		(number of iterations between 0 and < 256000, it is
//						 stored as iterations/1024)
//		salt			(a random octet string, at most 256 byte long )
func NewSEKPacket(KDFmode KDFMode, iterations int, salt []byte) (sekp SymmetricEncryptionKeyPacket, err error) {
	sekp = SymmetricEncryptionKeyPacket{
		packetHeader: packetHeader{
			Version: 0x1,
			Tag:     PTAGSymmetricEncryptionKey,
		},
		KDFMode: KDFmode.GetNID(),
	}

	// store iterations / 2^10
	iter := iterations >> 10
	if iter < 0 || iter > 255 {
		err = fmt.Errorf("Invalid number of iteration count: %d", iterations)
		return
	}
	sekp.Iterations = uint8(iter)

	if len(salt) > 256 {
		err = fmt.Errorf("Salt too long: %d B (exceeds 256 B)", len(salt))
		return
	}
	sekp.SaltLength = uint8(len(salt))
	sekp.Salt = salt

	sekp.BodyLength = 2 + 1 + 1 + varUInt(sekp.SaltLength)
	sekp.adjustHLength()

	return
}
