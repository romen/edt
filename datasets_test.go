package edt

type newSEKPDataType struct {
	KDFnid NID
}

type newSEKDDataType struct {
	Passphrase string
	Plaintext  string
	AEADnid    NID
	KDFnid     NID
}

var SEKPTests = []newSEKPDataType{
	{NID_PBKDF2_SHA3_512},
}

var SEDPTests = []newSEKDDataType{
	// GCM_AES128
	{"random passphrase", "random plaintext", NID_GCM_AES128, NID_PBKDF2_SHA3_512},
	{"password", "Hello World!!", NID_GCM_AES128, NID_PBKDF2_SHA3_512},
	{"password", "vim-go ⌘ ⌘ ⌘ can you read me??", NID_GCM_AES128, NID_PBKDF2_SHA3_512},
	{"パスワード", "vim-go ⌘ ⌘ ⌘ can you read me??", NID_GCM_AES128, NID_PBKDF2_SHA3_512},
	{"暗証", "vim-go ⌘ ⌘ ⌘ can you read me??", NID_GCM_AES128, NID_PBKDF2_SHA3_512},
	{"暗号", "vim-go ⌘ ⌘ ⌘ can you read me??", NID_GCM_AES128, NID_PBKDF2_SHA3_512},
	{"合言葉", "vim-go ⌘ ⌘ ⌘ can you read me??", NID_GCM_AES128, NID_PBKDF2_SHA3_512},
	{"合い言葉", "vim-go ⌘ ⌘ ⌘ can you read me??", NID_GCM_AES128, NID_PBKDF2_SHA3_512},
	{"符丁", "vim-go ⌘ ⌘ ⌘ can you read me??", NID_GCM_AES128, NID_PBKDF2_SHA3_512},
	// GCM_AES256
	{"random passphrase", "random plaintext", NID_GCM_AES256, NID_PBKDF2_SHA3_512},
	{"password", "Hello World!!", NID_GCM_AES256, NID_PBKDF2_SHA3_512},
	{"password", "vim-go ⌘ ⌘ ⌘ can you read me??", NID_GCM_AES256, NID_PBKDF2_SHA3_512},
	{"パスワード", "vim-go ⌘ ⌘ ⌘ can you read me??", NID_GCM_AES256, NID_PBKDF2_SHA3_512},
	{"暗証", "vim-go ⌘ ⌘ ⌘ can you read me??", NID_GCM_AES256, NID_PBKDF2_SHA3_512},
	{"暗号", "vim-go ⌘ ⌘ ⌘ can you read me??", NID_GCM_AES256, NID_PBKDF2_SHA3_512},
	{"合言葉", "vim-go ⌘ ⌘ ⌘ can you read me??", NID_GCM_AES256, NID_PBKDF2_SHA3_512},
	{"合い言葉", "vim-go ⌘ ⌘ ⌘ can you read me??", NID_GCM_AES256, NID_PBKDF2_SHA3_512},
	{"符丁", "vim-go ⌘ ⌘ ⌘ can you read me??", NID_GCM_AES256, NID_PBKDF2_SHA3_512},
}
