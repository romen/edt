package edt

import (
	"reflect"
	"testing"
)

func Test_opaquePacket_Parse_from_SEKP(t *testing.T) {
	for _, td := range SEKPTests {
		sekp, err := newSEKPFromTestData(td, t)

		b := sekp.Serialize()

		var op opaquePacket
		n, err := op.Parse(b)
		if err != nil {
			t.Error(
				"For op.Parse(", b, ")",
				"got error", err,
			)
			return
		}

		if n < phMinLength {
			t.Error("Wrong parsed length")
			return
		}

		eq := reflect.DeepEqual(op.packetHeader, sekp.packetHeader)

		if eq != true {
			t.Error(
				"For", "op.packetHeader",
				"expected", sekp.packetHeader,
				"got", op.packetHeader,
			)
		}

	}
}

func Test_opaquePacket_Parse_from_SEDP(t *testing.T) {
	for _, td := range SEDPTests {
		_, sedp, err := newSEDPFromTestData(td, t)

		b := sedp.Serialize()

		var op opaquePacket
		n, err := op.Parse(b)
		if err != nil {
			t.Error(
				"For op.Parse(", b, ")",
				"got error", err,
			)
			return
		}

		if n < phMinLength {
			t.Error("Wrong parsed length")
			return
		}

		eq := reflect.DeepEqual(op.packetHeader, sedp.packetHeader)

		if eq != true {
			t.Error(
				"For", "op.packetHeader",
				"expected", sedp.packetHeader,
				"got", op.packetHeader,
			)
		}
	}
}

func Test_ParsePacket_SEKP(t *testing.T) {
	for _, td := range SEKPTests {
		sekp, err := newSEKPFromTestData(td, t)

		b := sekp.Serialize()
		n, p, err := ParsePacket(b)
		if err != nil {
			t.Error(
				"For ParsePacket(", sekp.Serialize(), ")",
				"got error", err,
			)
		} else if n != uint64(len(b)) {
			t.Error(
				"For ParsePacket(", sekp.Serialize(), ")",
				"expected bytecount", len(b),
				"got", n,
			)
		} else if eq := reflect.DeepEqual(&sekp, p); eq != true {
			t.Error(
				"For ParsePacket(", sekp.Serialize(), ")",
				"expected", &sekp,
				"got", p,
			)
		}
	}
}

func Test_ParsePacket_SEDP(t *testing.T) {
	for _, td := range SEDPTests {
		_, sedp, err := newSEDPFromTestData(td, t)

		b := sedp.Serialize()
		n, p, err := ParsePacket(b)

		if err != nil {
			t.Error(
				"For ParsePacket(", sedp.Serialize(), ")",
				"got error", err,
			)
		} else if n != uint64(len(b)) {
			t.Error(
				"For ParsePacket(", sedp.Serialize(), ")",
				"expected bytecount", len(b),
				"got", n,
			)
		} else if eq := reflect.DeepEqual(&sedp, p); eq != true {
			t.Error(
				"For ParsePacket(", sedp.Serialize(), ")",
				"expected", &sedp,
				"got", p,
			)
		}
	}
}

func Test_SEDP_SelfOpen(t *testing.T) {
	for _, td := range SEDPTests {
		sekp, sedp, err := newSEDPFromTestData(td, t)

		decrypted, err := sedp.Open(td.Passphrase, &sekp)
		if err != nil {
			t.Error(
				"For sedp.Open(", td.Passphrase, &sekp, ")",
				"got error", err,
			)
			return
		}

		if string(decrypted) != td.Plaintext {
			t.Error(
				"For", td,
				"expected", td.Plaintext,
				"got", decrypted,
			)
		}
	}
}
